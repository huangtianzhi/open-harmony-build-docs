### Class NotificationManager中的各个事件及其含义

#### Overall：

NotificationManager 作为运行时的事件通知管理类，负责管理由解释器interpreter在解释运行字节码时通知运行时实例的各种事件。整体流程如下：

1. 解释器在调试模式下解释运行字节码时，通过运行不同指令对应的handler方法/显式调用向运行时实例EcmaVM通知事件。
2. 通过EcmaVM获取当前调试信息管理类JsDebuggerManager, 其中包含包括NotificationManager在内的调试相关的信息和管理类。
3. 通过JsDebuggerManager获取到运行时事件通知管理类NotificationManager，并调用NotificationManager公共方法中与当前事件对应的Event handle方法。
4. NotificationManager中的公共方法对应各自handle的运行时事件，并通过调用NotificationManager中的私有对象RuntimeListener运行时监听类对应的方法，继而调用监听类的继承类JsDebugger中对应的方法进行事件回调分发。
5. JsDebugger通过调用JSDebugInterface的实现类，PtHooks的继承类JsPtHooks中对应事件的方法触发运行时事件的回调，继而调用toolchain中debugger的实现类debuggerImpl中的方法来具体实现。

在这个流程中，NotificationManager 根据EcmaVM接收到的事件通知，根据事件的类型，调用NotificationManager 中相对应的handle方法；对外作为运行时事件相应处理的接口，并由其中的监听类继续分发事件回调，直至最后实现。

#### Public methods：

##### --AddListener: 

​	注册绑定NotificationManager中的私有对象RuntimeListener。

##### --RemoveListener:

​	移除NotificationManager中的RuntimeListener。

##### --LoadModuleEvent: 

​	加载JsPandaFile后的事件，当JsPandaFile已经被loaded后，在JSPandaFileExecutor执行execute时，通过InvokeEcmaEntrypoint调用解释器execute前；在js_cjs_module里的RequireExecution中execute执行前；在热重载中ExtractPatch抓取patch方法里，该事件都会被通知，后续通过回调方法实现ws发送Debugger.scriptParsed回复中的参数获取等具体实现。

##### --BytecodePcChangedEvent:

​	字节码执行位置改变事件，interpreter frame中pc指针指示的是当前字节码执行位置，即bytecode address，当解释器执行下一句字节码时（或者是单步调试时），pc指针会改变到下一句的开始位置，此时该事件就会被通知。

##### --DebuggerStmtEvent:

​	执行debugger语句时通知的事件，当解释器当前执行的是debugger语句时，会通过DebuggerStmtEvent最终实现运行暂停在debugger语句处的结果。

##### --NativeCallingEvent:

​	执行 native / builtin方法前通知的事件，在执行NativeFunctionCallBack中，判断如果当前混合调试模式开启，就会通知NativeCalling事件，通过ArkNativeEngine --> JSNApi --> NotificationManager调用该方法。

##### --PendingJobEntryEvent: 

​	执行 micro job前通知的事件，在InvokeEcmaEntrypoint中，当判断当前没有待处理异常时，就会从微任务队列中取出PromiseJobQueue和ScriptJobQueue，循环pop取出待执行job，在执行该job之前调用该方法通知该事件。

##### --VmStartEvent:

​	运行时实例启动时事件，在JsDebugger类注册了事件回调类PtHooks后显式调用，后续没有具体代码实现（在JsPtHooks中为空实现）

##### --VmDeathEvent:

​	运行时实例死亡时事件，在JsDebugger类移除已注册的事件回调类PtHooks之前显式调用，后续没有具体代码实现（在JsPtHooks中为	空实现）

#### Private variable:

##### --RuntimeListener: 

​	运行时监听类对象，NotificationManager通过其实现类JsDebugger实现运行时事件回调分发，注册运行时事件回调类等。