# OpenHarmony-Build-Docs

#### Author

Tianzhi Huang (huangtianzhi1@huawei.com)

#### Description

Documenting problems and errors encountered during every phase through out the OpenHarmony development and programming, and providing possible troubleshooting analysis and fix.

#### Revision History
2023-06-27      [add variable value showing when in debug mode learning log](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/pre-job_mentoring_phase2/%E8%B0%83%E8%AF%95%E6%A8%A1%E5%9D%97%E5%8F%98%E9%87%8F%E5%B1%95%E7%A4%BA%E5%8A%9F%E8%83%BD%E5%86%85%E9%83%A8%E9%80%BB%E8%BE%91.md)

2023-06-27      [add notification manager learning logs](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/pre-job_mentoring_phase2/Class%20NotificationManager%E4%B8%AD%E7%9A%84%E5%90%84%E4%B8%AA%E4%BA%8B%E4%BB%B6%E5%8F%8A%E5%85%B6%E5%90%AB%E4%B9%89.md)

2023-06-13	[add compile and test related commands](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/OpenHarmony%20rk3568%20Compiling%20and%20Testing%20Commands.md)

2023-05-24      [add errors & problems in download and built phase](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/Errors%20&%20Problems%20--%20Code%20Download%20and%20Compilation.md)

2023-05-24	update README.en

2023-05-24	initial setup

#### Disclaim

This Git Inventory is created solely for studying purpose and the owner of the inventory owns the copyright of every file contained, please contact 137698573@qq.com or cite the original source before republishing.
