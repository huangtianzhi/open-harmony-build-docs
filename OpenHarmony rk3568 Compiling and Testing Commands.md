# OpenHarmony rk3568 Compiling and Testing Commands

#### 1. rk3569 image .so compilation

##### 32-bits

```bash
./build.sh --product-name rk3568
```

##### 64-bits

```bash
./build.sh --product-name rk3568 --gn-args use_musl=true --target-cpu arm64
```

##### Most used when developing JsRuntime / toolchain

```bash
./build.sh --product-name rk3568 --build-target ark_js_host_linux_tools_packages --build-target ets_frontend_build --build-target libark_ecma_debugger
```

#### 2. Unit/262 tests execution 

##### unit test

```bash
./build.sh --product-name rk3568 --build-target ark_js_host_unittest --build-target ark_js_unittest --build-target ark_js_host_linux_tools_packages
```

##### 262 test cases

```bash
./build.sh --product-name rk3568 --build-target ark_js_host_linux_tools_packages --build-target ets_frontend_build

cd arkcompiler/ets_frontend

python3 test262/run_test262.py --es2021 all --libs-dir ../../out/rk3568/clang_x64/arkcompiler/ets_runtime:../../out/rk3568/clang_x64/thirdparty/icu:../../out/rk3568/clang_x64/thirdparty/zlib:../../out/rk3568/clang_x64/thirdparty/cjson:../../prebuilts/clang/ohos/linux-x86_64/llvm/lib --ark-tool=../../out/rk3568/clang_x64/arkcompiler/ets_runtime/ark_js_vm --ark-frontend-binary=../../out/rk3568/clang_x64/arkcompiler/ets_frontend/es2abc --merge-abc-binary=../../out/rk3568/clang_x64/arkcompiler/ets_frontend/merge_abc --ark-frontend=es2panda
```

#### 3. rk3568 image .so push and build

##### 1. compile code base

```bash
./build.sh --product-name rk3568 --build-target ark_js_host_linux_tools_packages --build-target ets_frontend_build --build-target libark_ecma_debugger
```

##### 2. change directory to the one contains built .so 

```bash
cd \\wsl.localhost\Ubuntu-20.04\home\XXX\OpenHarmony\master\out\rk3568\arkcompiler\ets_runtime
```

##### 3. push .so to rk3568 developing pad

```bash
hdc shell mount -o remount,rw / 
hdc file send libark_jsruntime.so /system/lib
```

##### 4. reboot the rk3568

```bash
hdc shell reboot
```

#### 4. pull most recent code from weekly built and apply personal modification

##### 1. save local uncommitted changes

```bash
git stash -m "XXXXXX"
```

##### 2. download [**dayu200**] images from the weekly built

```http
http://ci.openharmony.cn/workbench/cicd/dailybuild/dailylist
```

##### 3. copy [manifest_tag.xml] into local branch ./repo/manifests

```bash
cp manifest_tag.xml .repo/manifests/
```

##### 4. synchronize local code base with downloaded

```bash
repo sync -c -m manifest_tag.xml
```

##### 5. apply your local code changes to the updated code base

```bash
git stash pop
```

#### 6. Using chrome devtool to debug

```http
https://gitee.com/Gymee/ark_js_runtime/wikis/%E4%BD%BF%E7%94%A8chrome%E8%B0%83%E8%AF%95
```

#### 7. Other useful developing commands

```http
https://gitee.com/Gymee/ark_js_runtime/wikis/%E5%B8%B8%E7%94%A8%E5%BC%80%E5%8F%91%E5%91%BD%E4%BB%A4/%E8%AE%BE%E5%A4%87%E4%BE%A7
```

##### 

#### 8. generate .abc

```bash
out/rk3568/clang_x64/arkcompiler/ets_frontend/es2abc test1.js
out/rk3568/clang_x64/arkcompiler/runtime_core/ark_disasm test1.abc output.pa
```

#####

