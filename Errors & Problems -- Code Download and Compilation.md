# Errors & Problems -- Code Download and Compilation

#### 1. Encounter error: [...(-9): decoding TLS package Error]

##### Possible reason

Git download connection unstable, insufficient git download buffer, MTU (Maximum Transmission Unit) too small

##### Fix

```bash
git config --global http.sslVerify false
git config --global http.postBuffer 1048576000 # 1GB buffer
git config --global http.lowSpeedLimit 0
git config --global http.lowSpeedLTime 999999

sudo ifconfig eth0 mtu 14000
```

#### 2. VS Code connects WSL2 Ubuntu fail: [.../wslServer.sh: Permission denied]

##### Possible reason

VS Code does not have the proper permission on wslServer.sh file

##### Fix

```bash
cd /mnt/c/Users/[userName]/.vscode/extensions/ms-vscode.remote-wsl-0.42.3/scripts
chmod +x *
```

Then reconnect the WSL through VS Code

#### 3. Encounter error when executing [./build/prebuilts_download.sh] to download packages necessary to build the project : [Cannot find module "xxx"] (including semver, uuid... and other 50+ modules are missing in [node-v14.21.1 dir], which is provided)

##### Possible reason

The provided node-v14.21.1 is missing those modules inside its node_modules dir

or

The verison of node pre-installed in the Linux subsystem conflicts with the one provided and used in the project

##### Fix

```bash
cd ~/OpenHarmony/master/prebuilts/build-tools/common/nodejs/node-v14.21.1-linux-x64/lib/node_modules/npm/node_modules
npm install semver
... # install all missing modules through npm install command
```

or 

```bash
npm config set registry https://registry.npm.taobao.org # switch npm registry
npm install . # re-installed all modules
```

#### 4.Encounter error when compiling project by executing [./build.sh --product-name rk3568 --ccache]: 4000, ninja phase failed / 3000, gn phase failed

##### Possible reason

OpenHarmony master branch unstable; ninja or gn not properly installed.

##### Fix

```bash
sudo apt install ninja / gn
cp ninja /usr/bin/ninja
cp gn /usr/bin/gn
```

