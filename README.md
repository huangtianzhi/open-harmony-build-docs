# OpenHarmony-Build-Docs

#### 作者
黄天质（huangtianzhi1@huawei.com）

#### 介绍

本人对OpenHarmony环境搭建及开发中碰到的Errors和问题的记录，并附上可能的错误原因分析及问题解决办法

#### 更新历史
2023-06-27      [更新调试变量展示学习记录](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/pre-job_mentoring_phase2/%E8%B0%83%E8%AF%95%E6%A8%A1%E5%9D%97%E5%8F%98%E9%87%8F%E5%B1%95%E7%A4%BA%E5%8A%9F%E8%83%BD%E5%86%85%E9%83%A8%E9%80%BB%E8%BE%91.md)

2023-06-27      [更新NotificationManager学习记录](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/pre-job_mentoring_phase2/Class%20NotificationManager%E4%B8%AD%E7%9A%84%E5%90%84%E4%B8%AA%E4%BA%8B%E4%BB%B6%E5%8F%8A%E5%85%B6%E5%90%AB%E4%B9%89.md)

2023-06-13	[更新编译及开发命令](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/OpenHarmony%20rk3568%20Compiling%20and%20Testing%20Commands.md)

2023-05-24      [更新编译和构建中可能遇到的问题](https://gitee.com/huangtianzhi/open-harmony-build-docs/blob/master/Errors%20&%20Problems%20--%20Code%20Download%20and%20Compilation.md)

2023-05-24	更新仓库说明

2023-05-24	初始构建及设置

#### 声明

本仓库下所有文件均由本人以个人学习提升的目的创建，所有权归作者所有。任何人在联系137698573@qq.com征得作者同意或者标明出处之前不允许私自转载。
